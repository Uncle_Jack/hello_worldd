import java.util.Scanner;
public class Strings {
    public static void main(String[] args) {

        String BadWord[] = {"Бяка","бяка", "лох", "Лох", "дебил", "Дебил", };

        Scanner in = new Scanner(System.in);
        String str1 = in.nextLine();

        String[] strmass = str1.split(" ");

        for (String element: strmass) {
            for (int i = 0; i < BadWord.length; i++) {
                if (element.equals(BadWord[i])) {
                    element = "Вырезано цензурой";
                }
            }
            System.out.print(element + " ");
        }

    }
}